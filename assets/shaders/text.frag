#version 330 core

in vec3 pass_color;
in vec2 pass_uv;

out vec4 fragColor;

uniform sampler2D tex;

void main() {
	fragColor = vec4(pass_color, texture(tex, pass_uv).a);
}