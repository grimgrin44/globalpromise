#version 330 core

in vec2 texCoord;

out vec4 fragColor;

uniform sampler2D tex;

void main() {
	vec4 c = texture(tex, texCoord);
	//if(c.a < 0.01) {
	//	discard;
	//}
	fragColor = c;
}