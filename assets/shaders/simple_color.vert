#version 330 core

layout (location = 0) in vec2 in_pos;
layout (location = 1) in vec3 in_color;

out vec3 fragColor;

uniform mat4 projection;

void main() {
	gl_Position = projection * vec4(in_pos, 0.0, 1.0);
	fragColor = in_color;
}