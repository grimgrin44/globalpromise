#version 330 core

layout (location = 0) in vec2 in_pos;
layout (location = 1) in vec2 in_uv;

out vec3 pass_color;
out vec2 pass_uv;

uniform mat4 projection;
uniform vec3 color;

void main() {
	gl_Position = projection * vec4(in_pos, 0.0, 1.0);
	pass_color = color;
	pass_uv = in_uv;
}