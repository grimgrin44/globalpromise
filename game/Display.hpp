#pragma once

#include <iostream>
#include <string>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

class Display {

public:

	static Display* createDisplay(const std::string& title, int width, int height);
	static Display* getDisplay();
	static void destroy();

	GLFWwindow* getWindow() const;
	int getWidth() const;
	int getHeight() const;

protected:

	GLFWwindow* m_window;
	int m_width;
	int m_height;

	static Display* m_instance;
};

