/*
 * Image.hpp
 *
 *  Created on: 2020. m�rc. 1.
 *      Author: korme
 */

#ifndef GAME_TEXTURES_IMAGE_HPP_
#define GAME_TEXTURES_IMAGE_HPP_

#include <string>
#include <utility/ImageUtils.hpp>

class Image {

public:

	Image(const std::string& path);
	~Image();

	const unsigned char* getImageData() const;
	const Utils::ImageSize& getImageSize() const;

protected:

	Utils::ImageSize m_size;
	unsigned char* m_imgData = nullptr;

};



#endif /* GAME_TEXTURES_IMAGE_HPP_ */
