/*
 * Image.cpp
 *
 *  Created on: 2020. m�rc. 1.
 *      Author: korme
 */

#include "Image.hpp"
#include <vector>


Image::Image(const std::string& path) {
	std::vector<unsigned char> d = Utils::ImageUtils::loadPNG(path, m_size);
	m_imgData = new unsigned char[d.size()];
	std::copy(d.begin(), d.end(), m_imgData);
}

Image::~Image() {
	delete m_imgData;
}

const unsigned char* Image::getImageData() const {
	return m_imgData;
}

const Utils::ImageSize& Image::getImageSize() const {
	return m_size;
}
