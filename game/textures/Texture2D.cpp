/*
 * Texture2D.cpp
 *
 *  Created on: 2020. m�rc. 1.
 *      Author: korme
 */
#include "Texture2D.hpp"

Texture2D::Texture2D(const unsigned char* data, const Utils::ImageSize& size) {
	glGenTextures(1, &m_texture);
	glBindTexture(GL_TEXTURE_2D, m_texture);

	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, size.width, size.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	glGenerateMipmap(GL_TEXTURE_2D);
}

Texture2D::Texture2D(const Image* image) : Texture2D(image->getImageData(), image->getImageSize()) {

}

Texture2D::~Texture2D() {
	glDeleteTextures(1, &m_texture);
}

void Texture2D::bind() const {
	glBindTexture(GL_TEXTURE_2D, m_texture);
}

