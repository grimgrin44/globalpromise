/*
 * Texture2D.hpp
 *
 *  Created on: 2020. m�rc. 1.
 *      Author: korme
 */

#ifndef GAME_TEXTURES_TEXTURE2D_HPP_
#define GAME_TEXTURES_TEXTURE2D_HPP_

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <utility/ImageUtils.hpp>

#include "Image.hpp"

class Texture2D {

public:

	Texture2D(const unsigned char* data, const Utils::ImageSize& size);
	Texture2D(const Image* image);
	~Texture2D();

	void bind() const;

protected:

	GLuint m_texture;

};



#endif /* GAME_TEXTURES_TEXTURE2D_HPP_ */
