#include "Display.hpp"

Display* Display::m_instance = nullptr;

Display* Display::createDisplay(const std::string& title, int width, int height) {

	if (!m_instance) {
		m_instance = new Display();
		m_instance->m_window = glfwCreateWindow(width, height, "Global Promise: The game", NULL, NULL);
		m_instance->m_width = width;
		m_instance->m_height = height;
		if (!m_instance->m_window) {
			std::cerr << "Failed to create window. Exiting." << std::endl;
		}
	}

	return m_instance;
}

void Display::destroy() {
	glfwDestroyWindow(m_instance->m_window);
	delete m_instance;
	m_instance = nullptr;
}

Display* Display::getDisplay() {
	return m_instance;
}

GLFWwindow* Display::getWindow() const {
	return m_instance->m_window;
}

int Display::getWidth() const {
	return m_width;
}

int Display::getHeight() const {
	return m_height;
}