/*
 * BoundingBox.hpp
 *
 *  Created on: 2020. m�rc. 8.
 *      Author: korme
 */

#ifndef GAME_SHAPES_BOX_HPP_
#define GAME_SHAPES_BOX_HPP_

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>


class Box {

public:

	/**
	 * Structure containing position and color information
	 */
	struct Vertices {
		float x1, y1, r1, g1, b1;
		float x2, y2, r2, g2, b2;
		float x3, y3, r3, g3, b3;
		float x4, y4, r4, g4, b4;
	};

	Box(const glm::vec2& position, float width, float height, const glm::vec3& color);
	~Box();

	void setWidth(float width);
	void setHeight(float height);
	void transform(const glm::mat4& transform);

	void getVertices(Vertices* vertices);

protected:


	glm::vec2 m_position;
	float m_width = 0.0f;
	float m_height = 0.0f;
	glm::vec3 m_color;

	Vertices m_vertices;


};



#endif /* GAME_SHAPES_BOX_HPP_ */
