/*
 * Triangle.cpp
 *
 *  Created on: 2020. m�rc. 1.
 *      Author: korme
 */

#include "Triangle.hpp"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

Triangle::Triangle(glm::vec3 v1, glm::vec3 v2, glm::vec3 v3, glm::vec3 color) {

	float vertices[18];

	vertices[0] = v1.x, vertices[1] = v1.y, vertices[2] = v1.z;
	vertices[3] = color.x; vertices[4] = color.y; vertices[5] = color.z;

	vertices[6] = v2.x, vertices[7] = v2.y, vertices[8] = v2.z;
	vertices[9] = color.x; vertices[10] = color.y; vertices[11] = color.z;

	vertices[12] = v3.x, vertices[13] = v3.y, vertices[14] = v3.z;
	vertices[15] = color.x; vertices[16] = color.y; vertices[17] = color.z;

	glGenVertexArrays(1, &m_vao);
	glBindVertexArray(m_vao);

	glGenBuffers(1, &m_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*) 0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*) (3 * sizeof(float)));
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	glBindVertexArray(0);
}

Triangle::~Triangle() {

}

void Triangle::render() {
	glBindVertexArray(m_vao);
	glDrawArrays(GL_TRIANGLES, 0, 3);
	glBindVertexArray(0);
}
