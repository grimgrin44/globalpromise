/*
 * Triangle.hpp
 *
 *  Created on: 2020. m�rc. 1.
 *      Author: korme
 */

#ifndef GAME_SHAPES_TRIANGLE_HPP_
#define GAME_SHAPES_TRIANGLE_HPP_

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>

class Triangle {

public:

	Triangle(glm::vec3 v1, glm::vec3 v2, glm::vec3 v3, glm::vec3 color);
	~Triangle();

	void render();

protected:

	GLuint m_vao;
	GLuint m_vbo;

};



#endif /* GAME_SHAPES_TRIANGLE_HPP_ */
