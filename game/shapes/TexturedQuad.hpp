/*
 * TexturedQuad.hpp
 *
 *  Created on: 2020. m�rc. 1.
 *      Author: korme
 */

#ifndef GAME_SHAPES_TEXTUREDQUAD_HPP_
#define GAME_SHAPES_TEXTUREDQUAD_HPP_

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <game/textures/Texture2D.hpp>

class TexturedQuad {

public:

	TexturedQuad(unsigned int width, unsigned int height, Texture2D& texture);
	virtual ~TexturedQuad();

	void render();

protected:

	unsigned int m_width;
	unsigned int m_height;
	Texture2D& m_texture;

	GLuint m_vao;
	GLuint m_vbo[2];

	float m_vertices[20];

	void generateVertices();


};


#endif /* GAME_SHAPES_TEXTUREDQUAD_HPP_ */
