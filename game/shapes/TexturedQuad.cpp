/*
 * TexturedQuad.cpp
 *
 *  Created on: 2020. m�rc. 1.
 *      Author: korme
 */

#include "TexturedQuad.hpp"

TexturedQuad::TexturedQuad(unsigned int width, unsigned int height, Texture2D& texture) :
	m_width(width), m_height(height), m_texture(texture) {
	generateVertices();

	glGenVertexArrays(1, &m_vao);
	glBindVertexArray(m_vao);

	glGenBuffers(2, m_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(m_vertices), m_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*) 0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*) (3 * sizeof(float)));
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	unsigned int indices[6] = {
		0, 1, 2,
		2, 3, 0
	};

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_vbo[1]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	glBindVertexArray(0);
}

TexturedQuad::~TexturedQuad() {
	glDeleteBuffers(2, m_vbo);
	glDeleteVertexArrays(1, &m_vao);
}

void TexturedQuad::render() {
	glBindVertexArray(m_vao);
	m_texture.bind();
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

void TexturedQuad::generateVertices() {
	// Bottom left
	m_vertices[0] = -1.0f * m_width / 2.0f;
	m_vertices[1] = -1.0f * m_height / 2.0f;
	m_vertices[2] = 0.0f;

	m_vertices[3] = 0.0f;
	m_vertices[4] = 1.0f;

	// Bottom right
	m_vertices[5] = m_width / 2.0f;
	m_vertices[6] = -1.0f * m_height / 2.0f;
	m_vertices[7] = 0.0f;

	m_vertices[8] = 1.0f;
	m_vertices[9] = 1.0f;

	// Top right
	m_vertices[10] = m_width / 2.0f;
	m_vertices[11] = m_height / 2.0f;
	m_vertices[12] = 0.0f;

	m_vertices[13] = 1.0f;
	m_vertices[14] = 0.0f;

	// Top left
	m_vertices[15] = -1.0f * m_width / 2.0f;
	m_vertices[16] = m_height / 2.0f;
	m_vertices[17] = 0.0f;

	m_vertices[18] = 0.0f;
	m_vertices[19] = 0.0f;
}
