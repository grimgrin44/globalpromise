/*
 * Box.cpp
 *
 *  Created on: 2020. m�rc. 8.
 *      Author: korme
 */

#include "Box.hpp"

Box::Box(const glm::vec2 &position, float width, float height, const glm::vec3 &color) :
	m_position(position), m_width(width), m_height(height), m_color(color) {

}

Box::~Box() {

}

void Box::setWidth(float width) {
	m_width = width;
}

void Box::setHeight(float height) {
	m_height = height;
}

void Box::transform(const glm::mat4 &transform) {

}

void Box::getVertices(Vertices *vertices) {
	vertices->r1 = vertices->r2 = vertices->r3 = vertices->r4 = m_color.r;
	vertices->g1 = vertices->g2 = vertices->g3 = vertices->g4 = m_color.g;
	vertices->b1 = vertices->b2 = vertices->b3 = vertices->b4 = m_color.b;

	vertices->x1 = vertices->x4 = m_position.x - 1.0f * m_width / 2.0f;
	vertices->x2 = vertices->x3 = m_position.x + m_width / 2.0f;
	vertices->y3 = vertices->y4 = m_position.y + m_height / 2.0f;
	vertices->y1 = vertices->y2 = m_position.y - 1.0f * m_height / 2.0f;
}
