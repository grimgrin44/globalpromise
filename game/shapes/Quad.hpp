/*
 * Quad.hpp
 *
 *  Created on: 2020. m�rc. 3.
 *      Author: korme
 */

#ifndef GAME_SHAPES_QUAD_HPP_
#define GAME_SHAPES_QUAD_HPP_

#include <glad/glad.h>
#include <GLFW/glfw3.h>

class Quad {

public:

	Quad(unsigned int width, unsigned int height);
	~Quad();

	void render();

protected:

	unsigned int m_width;
	unsigned int m_height;

	GLuint m_vao;
	GLuint m_vbo[2];

	float m_vertices[20];

	void generateVertices();

};


#endif /* GAME_SHAPES_QUAD_HPP_ */
