/*
 * Background.cpp
 *
 *  Created on: 2020. m�rc. 3.
 *      Author: korme
 */

#include "Background.hpp"
#include <game/AssetManager.hpp>
#include <glm/gtc/matrix_transform.hpp>

#define WIDTH		4000

Background::Background() {
	AssetManager* am = AssetManager::getInstance();
	m_texture = am->getTexture("background");
	m_shader = am->getShader("background");
	m_quads[0] = new Quad(WIDTH + 1, 601);
	m_quads[1] = new Quad(WIDTH + 2, 601);

	m_x[0] = 0.0f;
	m_x[1] = WIDTH;

}

Background::~Background() {

}

void Background::setScrollSpeed(float v) {
	m_scrollSpeed = v;
}

void Background::update(float dt) {
	m_x[0] -= dt * m_scrollSpeed;
	m_x[1] -= dt * m_scrollSpeed;

	if(m_x[0] < m_x[1]) {
		if(m_x[0] < -(WIDTH + 10)) {
			m_x[0] += m_x[1] + 2 * WIDTH;
		}
	} else {
		if(m_x[1] < -(WIDTH + 10)) {
			m_x[1] += m_x[0] + 2 * WIDTH;
		}
	}
}

void Background::render() {
	m_shader->enable();
	m_texture->bind();
	m_shader->setUniform("projection", glm::ortho(0.0f, 1066.0f, 0.0f, 600.0f));

	glm::mat4 m = glm::identity<glm::mat4>();

	m_shader->setUniform("model", glm::translate(m, glm::vec3(m_x[0], 300, 0)));
	m_quads[0]->render();

	m_shader->setUniform("model", glm::translate(m, glm::vec3(m_x[1], 300, 0)));
	m_quads[1]->render();
}
