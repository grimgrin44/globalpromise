/*
 * Text.cpp
 *
 *  Created on: 2020. m�rc. 5.
 *      Author: korme
 */

#include "Text.hpp"
#include <game/AssetManager.hpp>

#include "TextRenderer.hpp"

Text::Text(const std::string& text, int x, int y, int size, Font* font) : m_text(text), m_font(font), m_x(x), m_y(y), m_size(size) {
	generateVertices();
	createVAO();
}

Text::Text(const std::string& text, int x, int y, int size, const std::string& font) : Text(text, x, y, size, AssetManager::getInstance()->getFont(font)) {

}

Text::~Text() {
	delete[] m_vertices;
	delete[] m_indices;
}

int Text::getWidth() const {
	return m_textWidth;
}

const Font* Text::getFont() const {
	return m_font;
}

const glm::vec3& Text::getColor() const {
	return m_color;
}

void Text::setText(const std::string& text) {
	if(text.length() > m_bufferCharCount) {
		m_text = text;
		generateVertices();
		glDeleteBuffers(2, m_vbo);
		glDeleteVertexArrays(1, &m_vao);
		createVAO();
	} else {
		m_text = text;
		generateVertices();
		updateVBO();
	}
}

void Text::setColor(float r, float g, float b) {
	m_color = glm::vec3(r, g, b);
}

void Text::setPosition(int x, int y) {
	m_x = x;
	m_y = y;
}

void Text::render() {
	glBindVertexArray(m_vao);
	glDrawElements(GL_TRIANGLES, 6 * m_text.length(), GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

void Text::createVAO() {

	if(m_indices) {
		delete[] m_indices;
	}

	m_bufferCharCount = m_text.length();

	glGenVertexArrays(1, &m_vao);
	glBindVertexArray(m_vao);

	glGenBuffers(2, m_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, m_text.length() * sizeof(TextQuadVertices), m_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*) 0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*) (2 * sizeof(float)));
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	int size = m_text.length() * 6;
	m_indices = new unsigned int[size];

	for(unsigned int i = 0; i < m_text.length(); i++) {
		int idx = i * 6;
		m_indices[idx++] = 0 + 4 * i;
		m_indices[idx++] = 1 + 4 * i;
		m_indices[idx++] = 2 + 4 * i;
		m_indices[idx++] = 2 + 4 * i;
		m_indices[idx++] = 3 + 4 * i;
		m_indices[idx++] = 0 + 4 * i;
	}

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_vbo[1]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, size * sizeof(unsigned int), m_indices, GL_STATIC_DRAW);
	glBindVertexArray(0);
}

void Text::updateVBO() {
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo[0]);
	glBufferSubData(GL_ARRAY_BUFFER, 0, m_text.length() * sizeof(TextQuadVertices), m_vertices);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Text::generateVertices() {
	if(m_vertices) {
		delete[] m_vertices;
	}
	m_vertices = new float[m_text.length() * sizeof(TextQuadVertices) / sizeof(float)];
	int m_xpos = m_x;
	m_textWidth = 0;
	TextQuadVertices* quad = (TextQuadVertices*) m_vertices;
	for(unsigned int i = 0; i < m_text.length(); i++) {
		char c = m_text[i];
		const Font::CharacterInfo* info = m_font->getCharacterInfo(c);
		quad->x1 = quad->x4 = (float) m_xpos + info->xoffset;
		quad->x2 = quad->x3 = (float) quad->x1 + info->width;

		quad->y3 = quad->y4 = (float) m_y + (m_size - info->yoffset);
		quad->y1 = quad->y2 = (float) quad->y3 - info->height;

		quad->u1 = info->u1;
		quad->u2 = info->u2;
		quad->u3 = info->u3;
		quad->u4 = info->u4;

		quad->v1 = info->v1;
		quad->v2 = info->v2;
		quad->v3 = info->v3;
		quad->v4 = info->v4;

		m_xpos += info->xadvance;// * 0.7f;
		m_textWidth += info->width;
		quad++;
	}
}
