/*
 * Font.h
 *
 *  Created on: 2020. m�rc. 4.
 *      Author: korme
 */

#ifndef GAME_TEXT_FONT_HPP_
#define GAME_TEXT_FONT_HPP_

#include <string>
#include <map>
#include <game/textures/Texture2D.hpp>
#include <utility/ImageUtils.hpp>

class Font {

public:

	struct CharacterInfo {
		int x, y;
		int width, height;
		int xoffset, yoffset;
		int xadvance;

		float ratio;

		float u1, v1;
		float u2, v2;
		float u3, v3;
		float u4, v4;
	};

	Font(const std::string& textureFile, const std::string& infoFile);
	virtual ~Font();

	int getDefaultFontHeight();
	const Texture2D* getTextureAtlas() const;

	const CharacterInfo* getCharacterInfo(char c);
	Utils::ImageSize getTextureAtlasSize();

protected:

	Texture2D* m_texture;
	Utils::ImageSize m_textureSize;
	int m_fontHeight = 0;

	std::map<unsigned char, CharacterInfo> m_characterInfos;

	void loadCharacterInfos(const std::string& infoFile);
	void parseInfoLine(char* line);
	void parseCharacterLine(char* line);
	void getKeyAndValue(char* str, char* key, char* value);

};

#endif /* GAME_TEXT_FONT_HPP_ */
