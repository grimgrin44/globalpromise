/*
 * Font.cpp
 *
 *  Created on: 2020. m�rc. 4.
 *      Author: korme
 */

#include "Font.hpp"
#include <game/textures/Image.hpp>
#include <utility/FileUtils.hpp>

#include <string.h>

#include <windows.h>

static char* strtok_r(char *str, const char *delim, char **nextp);

Font::Font(const std::string& textureFile, const std::string& infoFile) {
	Image img(textureFile);
	m_texture = new Texture2D(&img);
	m_textureSize = img.getImageSize();
	loadCharacterInfos(infoFile);
}

Font::~Font() {

}

int Font::getDefaultFontHeight() {
	return m_fontHeight;
}

const Texture2D* Font::getTextureAtlas() const {
	return m_texture;
}

const Font::CharacterInfo* Font::getCharacterInfo(char c) {
	return &m_characterInfos[c];
}

void Font::loadCharacterInfos(const std::string& infoFile) {
	m_characterInfos.clear();

	char* contents = Utils::loadFile(infoFile);
	if(!contents) {
		return;
	}

	char* savePtr;
	char* ptr = strtok_r(contents, "\r\n", &savePtr);
	while(ptr != NULL) {
		if(0 == strncmp(ptr, "info", 4)) {
			parseInfoLine(ptr);
		} else if(0 == strncmp(ptr, "char ", 5)) {
			parseCharacterLine(ptr);
		}
		ptr = strtok_r(NULL, "\r\n", &savePtr);
	}

	delete[] contents;
}

void Font::parseInfoLine(char* line) {
	char* savePtr;
	char* ptr = strtok_r(line, " ", &savePtr);

	if(0 != strcmp(ptr, "info")) {
		return;
	}

	ptr = strtok_r(NULL, " ", &savePtr);
	while(ptr) {
		char key[32] = { 0 };
		char value[32] = { 0 };

		getKeyAndValue(ptr, key, value);

		if(0 == strcmp(key, "face")) {
			std::cout << "Font face: " << value << std::endl;
		} else if(0 == strcmp(key, "size")) {
			m_fontHeight = atoi(value);
			std::cout << "Font size: " << value << std::endl;
		}

		ptr = strtok_r(NULL, " ", &savePtr);
	}
}

void Font::parseCharacterLine(char* line) {
	unsigned char id;
	CharacterInfo info = { 0 };
	char* savePtr;
	char* ptr = strtok_r(line, " ", &savePtr);
	if(0 != strcmp(ptr, "char")) {
		return;
	}

	ptr = strtok_r(NULL, " ", &savePtr);
	while(ptr) {
		char key[32] = { 0 };
		char value[32] = { 0 };

		getKeyAndValue(ptr, key, value);

		if(0 == strcmp(ptr, "id")) {
			id = atoi(value);
			//std::cout << "Found character ID: " << value << std::endl;
		} else if(0 == strcmp(ptr, "x")) {
			info.x = atoi(value);
		} else if(0 == strcmp(ptr, "y")) {
			info.y = atoi(value);
		} else if(0 == strcmp(ptr, "width")) {
			info.width = atoi(value);
		} else if(0 == strcmp(ptr, "height")) {
			info.height = atoi(value);
		} else if(0 == strcmp(ptr, "xoffset")) {
			info.xoffset = atoi(value);
		} else if(0 == strcmp(ptr, "yoffset")) {
			info.yoffset = atoi(value);
		} else if(0 == strcmp(ptr, "xadvance")) {
			info.xadvance = atoi(value);
		}

		info.ratio = (float) info.width / info.height;

		info.u1 = info.u4 = (float) info.x / m_textureSize.width;
		info.u2 = info.u3 = (float) (info.x + info.width) / m_textureSize.width;
		info.v1 = info.v2 = (float) (info.y + info.height) / m_textureSize.height;
		info.v3 = info.v4 = (float) info.y / m_textureSize.height;

		ptr = strtok_r(NULL, " ", &savePtr);
	}

	m_characterInfos[id] = info;
}

void Font::getKeyAndValue(char* str, char* key, char* value) {
	char* savePtr;
	char* ptr = strtok_r(str, "=", &savePtr);
	if(ptr) {
		strcpy_s(key, 32, ptr);
		ptr = strtok_r(NULL, "=", &savePtr);
		if(ptr) {
			strcpy_s(value, 32, ptr);
		}
	}
}

char* strtok_r(char *str, const char *delim, char **nextp) {
    char *ret;

    if (str == NULL) {
        str = *nextp;
    }

    str += strspn(str, delim);

    if (*str == '\0') {
        return NULL;
    }

    ret = str;

    str += strcspn(str, delim);

    if (*str) {
        *str++ = '\0';
    }

    *nextp = str;

    return ret;
}
