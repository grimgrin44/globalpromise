/*
 * TextRenderer.cpp
 *
 *  Created on: 2020. m�rc. 6.
 *      Author: korme
 */

#include "TextRenderer.hpp"
#include "Text.hpp"

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <game/AssetManager.hpp>
#include <game/Display.hpp>

TextRenderer::TextRenderer() {
	AssetManager* am = AssetManager::getInstance();
	m_shader = am->getShader("text");
	m_texts.clear();
}

TextRenderer::~TextRenderer() {

}

void TextRenderer::addText(Text* t) {
	m_texts.push_back(t);
}

void TextRenderer::render() {
	Display* display = Display::getDisplay();
	m_shader->enable();
	m_shader->setUniform("projection", glm::ortho(0.0f, (float) display->getWidth(), 0.0f, (float) display->getHeight()));
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	for(Text* t : m_texts) {
		t->getFont()->getTextureAtlas()->bind();
		m_shader->setUniform("color", t->getColor());
		t->render();
	}
	glDisable(GL_BLEND);
}


