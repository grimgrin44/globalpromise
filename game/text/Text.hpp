/*
 * Text.hpp
 *
 *  Created on: 2020. m�rc. 5.
 *      Author: korme
 */

#ifndef GAME_TEXT_TEXT_HPP_
#define GAME_TEXT_TEXT_HPP_

#include <string>
#include "Font.hpp"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

class TextRenderer;

enum class FontSize {
	FS_12px,
	FS_14px,
	FS_16px,
	FS_18px,
	FS_20px,
	FS_22px,
	FS_24px
};

class Text {

friend class TextRenderer;

public:

	Text(const std::string& text, int x, int y, int size, Font* font);
	Text(const std::string& text, int x, int y, int size, const std::string& font);
	~Text();

	const Font* getFont() const;
	const glm::vec3& getColor() const;

	void setText(const std::string& text);
	void setColor(float r, float g, float b);
	void setPosition(int x, int y);

	int getWidth() const;

	void render();

protected:

	struct TextQuadVertices {
		float x1, y1, u1, v1;
		float x2, y2, u2, v2;
		float x3, y3, u3, v3;
		float x4, y4, u4, v4;
	};

	std::string m_text;
	Font* m_font;
	glm::vec3 m_color{0.0f, 0.0f, 0.0f};
	float* m_vertices = nullptr;
	unsigned int* m_indices = nullptr;

	GLuint m_vao = 0;
	GLuint m_vbo[2] = { 0, 0 };

	int m_x = 0;
	int m_y = 0;
	int m_size = 32;
	int m_textWidth = 0;
	unsigned int m_bufferCharCount = 0;

	void generateVertices();
	void createVAO();
	void updateVBO();
};



#endif /* GAME_TEXT_TEXT_HPP_ */
