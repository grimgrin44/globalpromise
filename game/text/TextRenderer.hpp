/*
 * TextRenderer.hpp
 *
 *  Created on: 2020. m�rc. 5.
 *      Author: korme
 */

#ifndef GAME_TEXT_TEXTRENDERER_HPP_
#define GAME_TEXT_TEXTRENDERER_HPP_

#include <list>
#include <game/shader/Shader.hpp>
#include "Text.hpp"

class Text;

class TextRenderer {

public:

	TextRenderer();
	~TextRenderer();

	void addText(Text* t);

	void render();

protected:

	Shader* m_shader = nullptr;
	std::vector<Text*> m_texts = { 0 };

};


#endif /* GAME_TEXT_TEXTRENDERER_HPP_ */
