/*
 * AssetManager.cpp
 *
 *  Created on: 2020. m�rc. 2.
 *      Author: korme
 */

#include "AssetManager.hpp"

AssetManager* AssetManager::instance = nullptr;

AssetManager::~AssetManager() {
	for(std::pair<std::string, Texture2D*> p : m_textures) {
		delete p.second;
	}

	for(std::pair<std::string, Shader*> p : m_shaders) {
		delete p.second;
	}

	for(std::pair<std::string, Font*> p : m_fonts) {
		delete p.second;
	}

	m_textures.clear();
	m_shaders.clear();
	m_fonts.clear();
}

void AssetManager::init() {
	if(instance == nullptr) {
		instance = new AssetManager();
	}

	if(!instance->m_initialized) {
		instance->loadImages();
		instance->loadShaders();
		instance->loadFonts();
		instance->createTextures();
		instance->loadSounds();
	}
}

void AssetManager::destroy() {
	delete instance;
	instance = nullptr;
}

AssetManager* AssetManager::getInstance() {
	return instance;
}

AssetManager::AssetManager() {

}

Image* AssetManager::getImage(const std::string& name) {
	return m_images[name];
}

Shader* AssetManager::getShader(const std::string& name) {
	return m_shaders[name];
}

Texture2D* AssetManager::getTexture(const std::string& name) {
	return m_textures[name];
}

Font* AssetManager::getFont(const std::string& name) {
	return m_fonts[name];
}

OggFile* AssetManager::getOgg(const std::string& name) {
	return m_oggs[name];
}

void AssetManager::createTextures() {
	for(std::pair<std::string, Image*> p : m_images) {
		m_textures[p.first] = new Texture2D(p.second);
	}
}

void AssetManager::loadImages() {
	m_images["menu_go_one"] = new Image("assets/images/press_space_to_go_one.png");
	m_images["dragon_1"] = new Image("assets/images/dragon_1.png");
	m_images["dragon_2"] = new Image("assets/images/dragon_2.png");
	m_images["predator_1"] = new Image("assets/images/predator_1.png");
	m_images["predator_2"] = new Image("assets/images/predator_2.png");
	m_images["rat_1"] = new Image("assets/images/rat_1.png");
	m_images["rat_2"] = new Image("assets/images/rat_2.png");
	m_images["hero"] = new Image("assets/images/hero.png");
	m_images["kalyha"] = new Image("assets/images/kalyha.png");
	m_images["euro"] = new Image("assets/images/euro.png");
	m_images["background"] = new Image("assets/images/background.png");
	m_images["pig_1"] = new Image("assets/images/pig_1.png");
	m_images["pig_2"] = new Image("assets/images/pig_2.png");
	m_images["hero_run1"] = new Image("assets/images/hero_run1.png");
	m_images["hero_run2"] = new Image("assets/images/hero_run2.png");
	m_images["hero_run3"] = new Image("assets/images/hero_run3.png");
	m_images["hero_jump"] = new Image("assets/images/hero_jump.png");
}

void AssetManager::loadShaders() {
	m_shaders["simple_color"] = new Shader("assets/shaders/simple_color.vert", "assets/shaders/simple_color.frag");
	m_shaders["simple_texture"] = new Shader("assets/shaders/simple_texture.vert", "assets/shaders/simple_texture.frag");
	m_shaders["background"] = new Shader("assets/shaders/background.vert", "assets/shaders/background.frag");
	m_shaders["text"] = new Shader("assets/shaders/text.vert", "assets/shaders/text.frag");
}

void AssetManager::loadFonts() {
	m_fonts["consolas"] = new Font("assets/fonts/consolas/consolas14.png", "assets/fonts/consolas/consolas14.fnt");
}

void AssetManager::loadSounds() {
	m_oggs["theme"] = new OggFile("assets/sounds/theme.ogg");
	m_oggs["coin"] = new OggFile("assets/sounds/coin.ogg");
}
