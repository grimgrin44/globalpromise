/*
 * GameState.hpp
 *
 *  Created on: 2020. m�rc. 1.
 *      Author: korme
 */

#ifndef GAME_GAMESTATE_HPP_
#define GAME_GAMESTATE_HPP_

class GameState {

public:

	virtual ~GameState() {

	}

	virtual void update(float delta) = 0;
	virtual void render() = 0;
	virtual GameState* nextState() = 0;

};



#endif /* GAME_GAMESTATE_HPP_ */
