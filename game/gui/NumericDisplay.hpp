/*
 * NumericDisplay.hpp
 *
 *  Created on: 2020. m�rc. 8.
 *      Author: korme
 */

#ifndef GAME_GUI_NUMERICDISPLAY_HPP_
#define GAME_GUI_NUMERICDISPLAY_HPP_

#include <game/text/Text.hpp>

class NumericDisplay {

public:

	NumericDisplay(const std::string& label, int x, int y, FontSize size);
	~NumericDisplay();

	void setLabel(const std::string& label);
	void setValue(int value);

protected:

	std::string m_labelStr;

	int x;
	int y;
	Text* m_label;
	Text* m_text;

};



#endif /* GAME_GUI_NUMERICDISPLAY_HPP_ */
