/*
 * AssetManager.hpp
 *
 *  Created on: 2020. m�rc. 2.
 *      Author: korme
 */

#ifndef GAME_ASSETMANAGER_HPP_
#define GAME_ASSETMANAGER_HPP_

#include <map>
#include <string>
#include <game/textures/Image.hpp>
#include <game/textures/Texture2D.hpp>
#include <game/shader/Shader.hpp>
#include <game/text/Font.hpp>
#include <game/sound/OggFile.hpp>

class AssetManager {

public:

	~AssetManager();

	Image* getImage(const std::string& name);
	Shader* getShader(const std::string& name);
	Texture2D* getTexture(const std::string& name);
	Font* getFont(const std::string& name);
	OggFile* getOgg(const std::string& name);

	static void init();
	static void destroy();
	static AssetManager* getInstance();

protected:

	static AssetManager* instance;
	bool m_initialized = false;

	std::map<std::string, Image*> m_images;
	std::map<std::string, Texture2D*> m_textures;
	std::map<std::string, Shader*> m_shaders;
	std::map<std::string, Font*> m_fonts;
	std::map<std::string, OggFile*>  m_oggs;

	AssetManager();

	void loadImages();
	void loadShaders();
	void loadFonts();
	void loadSounds();
	void createTextures();

};



#endif /* GAME_ASSETMANAGER_HPP_ */
