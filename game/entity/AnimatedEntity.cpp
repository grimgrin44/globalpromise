/*
 * AnimatedEntity.cpp
 *
 *  Created on: 2020. m�rc. 2.
 *      Author: korme
 */

#include "AnimatedEntity.hpp"

AnimatedEntity::AnimatedEntity(unsigned int width, unsigned int height) : m_quad(width, height) {

}

AnimatedEntity::AnimatedEntity(Shader* shader, unsigned int width, unsigned int height) :
	m_shader(shader), m_quad(width, height) {

}

AnimatedEntity::~AnimatedEntity() {

}

void AnimatedEntity::render() {
	m_shader->enable();
	m_shader->setUniform("model", getModelMatrix());
	m_textures[m_index]->bind();
	m_quad.render();
}

void AnimatedEntity::next() {
	m_index++;
	if(m_index >= m_textures.size()) {
		m_index = 0;
	}
}

void AnimatedEntity::reset() {
	m_index = 0;
}

void AnimatedEntity::updateAnimation(float dt) {
	m_animationTime += dt;
	if(m_animationTime > m_animationDuration) {
		next();
		m_animationTime = 0.0f;
	}
}
