/*
 * StaticEntity.cpp
 *
 *  Created on: 2020. m�rc. 2.
 *      Author: korme
 */
#include "StaticEntity.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

StaticEntity::StaticEntity() {

}

StaticEntity::StaticEntity(Texture2D* texture, Shader* shader, unsigned int width, unsigned int height) : m_shader(shader) {
	m_object = new TexturedQuad(width, height, *texture);
}

StaticEntity::~StaticEntity() {
	delete m_object;
}

void StaticEntity::update(float dt) {
	// Do nothing, this is entity is static
}

void StaticEntity::render() {
	m_shader->enable();
	glm::mat4 model = getModelMatrix();
	m_shader->setUniform("model", model);
	m_object->render();
}



