/*
 * Pig.hpp
 *
 *  Created on: 2020. m�rc. 3.
 *      Author: korme
 */

#ifndef GAME_ENTITY_ENEMIES_PIG_HPP_
#define GAME_ENTITY_ENEMIES_PIG_HPP_

#include <game/entity/AnimatedEntity.hpp>

class Pig : public AnimatedEntity {

public:

	Pig();
	~Pig();

	void update(float dt) override;

};



#endif /* GAME_ENTITY_ENEMIES_PIG_HPP_ */
