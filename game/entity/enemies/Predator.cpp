/*
 * Predator.cpp
 *
 *  Created on: 2020. m�rc. 3.
 *      Author: korme
 */

#include "Predator.hpp"
#include <game/AssetManager.hpp>

Predator::Predator() : AnimatedEntity(AssetManager::getInstance()->getShader("simple_texture"), 256, 256) {
	AssetManager* am = AssetManager::getInstance();
	m_textures.push_back(am->getTexture("predator_1"));
	m_textures.push_back(am->getTexture("predator_2"));
	m_animationDuration = 0.6f;
}

Predator::~Predator() {

}

void Predator::update(float dt) {
	updateAnimation(dt);
}
