/*
 * Rat.hpp
 *
 *  Created on: 2020. m�rc. 3.
 *      Author: korme
 */

#ifndef GAME_ENTITY_ENEMIES_RAT_HPP_
#define GAME_ENTITY_ENEMIES_RAT_HPP_

#include <game/entity/AnimatedEntity.hpp>

class Rat : public AnimatedEntity {

public:

	Rat();
	~Rat();

	void update(float dt) override;

protected:

	float m_elapsed = 0.0f;

};



#endif /* GAME_ENTITY_ENEMIES_RAT_HPP_ */
