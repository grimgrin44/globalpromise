/*
 * Predator.hpp
 *
 *  Created on: 2020. m�rc. 3.
 *      Author: korme
 */

#ifndef GAME_ENTITY_ENEMIES_PREDATOR_HPP_
#define GAME_ENTITY_ENEMIES_PREDATOR_HPP_

#include <game/entity/AnimatedEntity.hpp>

class Predator : public AnimatedEntity  {

public:

	Predator();
	virtual ~Predator();

	void update(float dt) override;

};



#endif /* GAME_ENTITY_ENEMIES_PREDATOR_HPP_ */
