/*
 * Dragon.hpp
 *
 *  Created on: 2020. m�rc. 3.
 *      Author: korme
 */

#ifndef GAME_ENTITY_ENEMIES_DRAGON_HPP_
#define GAME_ENTITY_ENEMIES_DRAGON_HPP_

#include <game/AssetManager.hpp>
#include <game/entity/AnimatedEntity.hpp>

class Dragon : public AnimatedEntity {

public:

	Dragon();
	virtual ~Dragon();

	void update(float dt) override;

protected:

	float m_elapsed = 0.0f;
	float m_time = 0.0f;

	virtual void updateAnimation(float dt);

};


#endif /* GAME_ENTITY_ENEMIES_DRAGON_HPP_ */
