/*
 * Rat.cpp
 *
 *  Created on: 2020. m�rc. 3.
 *      Author: korme
 */

#include "Rat.hpp"
#include <game/AssetManager.hpp>
#include <game/Configuration.hpp>

Rat::Rat() : AnimatedEntity(AssetManager::getInstance()->getShader("simple_texture"), 128, 64) {
	AssetManager* am = AssetManager::getInstance();
	m_textures.push_back(am->getTexture("rat_1"));
	m_textures.push_back(am->getTexture("rat_2"));
	m_animationDuration = 0.15f;

	int r = rand() % 10;
	float s = 1.0f + (r / 10.0f);
	setScale(s);

	m_hitboxes.emplace_back(0, 0, 128 * s, 64 * s);
}

Rat::~Rat() {

}

void Rat::update(float dt) {
	updateAnimation(dt);

	m_x -= dt * RAT_SPEED;
}

