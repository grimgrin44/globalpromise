/*
 * Pig.cpp
 *
 *  Created on: 2020. m�rc. 3.
 *      Author: korme
 */

#include "Pig.hpp"
#include <game/AssetManager.hpp>
#include <game/Configuration.hpp>

Pig::Pig() : AnimatedEntity(AssetManager::getInstance()->getShader("simple_texture"), 150, 150) {
	AssetManager* am = AssetManager::getInstance();
	m_textures.push_back(am->getTexture("pig_1"));
	m_textures.push_back(am->getTexture("pig_2"));
	m_animationDuration = 0.5f;
	m_hitboxes.emplace_back(0, 0, 150, 150);
	m_velocity = -1.0f * PIG_SPEED;
}

Pig::~Pig() {

}

void Pig::update(float dt) {
	updateAnimation(dt);
	m_x += m_velocity *  dt;
}
