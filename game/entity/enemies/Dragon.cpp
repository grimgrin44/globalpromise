/*
 * Dragon.cpp
 *
 *  Created on: 2020. m�rc. 3.
 *      Author: korme
 */

#include "Dragon.hpp"
#include <game/Configuration.hpp>

Dragon::Dragon() : AnimatedEntity(AssetManager::getInstance()->getShader("simple_texture"),  256, 128) {
	AssetManager* am = AssetManager::getInstance();
	m_textures.push_back(am->getTexture("dragon_1"));
	m_textures.push_back(am->getTexture("dragon_2"));

	int r = rand() % 10;

	setScale(0.7f + (r / 10.0f));
	m_animationDuration = 0.8f;

	m_hitboxes.emplace_back(0, 0, 256 * m_scale_x * 0.8f, 128 * m_scale_y * 0.8f);
}

Dragon::~Dragon() {

}

void Dragon::updateAnimation(float dt) {
	m_animationTime += dt;
	if(m_index == 1) {
		if(m_animationTime >= (1.0f - m_animationDuration)) {
			next();
			m_animationTime = 0;
		}
	} else {
		if(m_animationTime >= m_animationDuration) {
			next();
			m_animationTime = 0;
		}
	}
}

void Dragon::update(float dt) {
	updateAnimation(dt);
	m_time +=  dt;

	m_x -= dt * DRAGON_SPEED;

	m_angle = 0.1f * sinf(m_time) + 0.3f;

}
