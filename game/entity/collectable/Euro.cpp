/*
 * Euro.cpp
 *
 *  Created on: 2020. m�rc. 7.
 *      Author: korme
 */

#include "Euro.hpp"

#include <game/AssetManager.hpp>

Euro::Euro() : AnimatedEntity(64, 64) {
	AssetManager* am = AssetManager::getInstance();
	m_shader = am->getShader("simple_texture");
	m_textures.push_back(am->getTexture("euro"));
	m_hitboxes.emplace_back(0, 0, 64, 64);
}

Euro::~Euro() {

}

void Euro::update(float dt) {
	m_x += m_velocity * dt;
}

void Euro::collect() {
	m_collected = true;
}



