/*
 * Collectable.hpp
 *
 *  Created on: 2020. m�rc. 7.
 *      Author: korme
 */

#ifndef GAME_ENTITY_COLLECTABLE_COLLECTABLE_HPP_
#define GAME_ENTITY_COLLECTABLE_COLLECTABLE_HPP_

class Collectable {

public:

	virtual ~Collectable() {

	}

	bool isCollected() const {
		return m_collected;
	}

	virtual void collect() = 0;

protected:

	bool m_collected = false;

};



#endif /* GAME_ENTITY_COLLECTABLE_COLLECTABLE_HPP_ */
