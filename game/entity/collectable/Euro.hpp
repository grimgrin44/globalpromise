/*
 * Euro.hpp
 *
 *  Created on: 2020. m�rc. 7.
 *      Author: korme
 */

#ifndef GAME_ENTITY_COLLECTABLE_EURO_HPP_
#define GAME_ENTITY_COLLECTABLE_EURO_HPP_

#include "../AnimatedEntity.hpp"
#include "Collectable.hpp"

class Euro : public Collectable, public AnimatedEntity {

public:

	Euro();
	virtual ~Euro();

	void update(float dt);
	void collect();


};



#endif /* GAME_ENTITY_COLLECTABLE_EURO_HPP_ */
