/*
 * Player.cpp
 *
 *  Created on: 2020. m�rc. 3.
 *      Author: korme
 */

#include "Player.hpp"
#include <game/AssetManager.hpp>
#include <game/Configuration.hpp>

Player::Player() : AnimatedEntity(100, 100) {
	AssetManager* am = AssetManager::getInstance();
	m_shader = am->getShader("simple_texture");
	m_textures.push_back(am->getTexture("hero_run1"));
	m_textures.push_back(am->getTexture("hero_run2"));
	m_textures.push_back(am->getTexture("hero_run3"));
	m_textures.push_back(am->getTexture("hero_jump"));
	m_animationDuration = 0.1f;

	m_hitboxes.emplace_back(0, 0, 60, 90);
}

Player::~Player() {

}

bool Player::inAir() const {
	return (m_y > PLAYER_Y_POS);
}

void Player::updateAnimation(float dt) {
	if(inAir()) {
		m_index = 3;
	} else {
		m_animationTime += dt;
		if(m_animationTime > m_animationDuration) {
			m_animationTime = 0;
			m_index++;
			if(m_index > 2) {
				m_index = 0;
			}
		}
	}
}

void Player::update(float dt) {
	m_time += dt;
	updateAnimation(dt);

	switch(m_jumpState) {

	case NO_JUMP:
		if(m_jump) {
			m_jumpState = JUMP;
			m_jumpStartTime = m_time;
			m_jump = false;
			m_velocity = JUMP_SPEED;
		} else {
			m_y = PLAYER_Y_POS;
			return;
		}
		break;

	case JUMP:
		if(m_jump) {
			m_jumpState = DOUBLE_JUMP;
			m_jumpStartTime = m_time;
			m_jump = false;
			m_velocity = JUMP_SPEED;
		}
		break;

	case DOUBLE_JUMP:
		break;

	default:
		m_jumpState = NO_JUMP;
		break;

	}

	float j_dt = m_time - m_jumpStartTime;
	float damping = m_y / 1300.0;

	m_y += m_velocity / (1 + damping) * j_dt + 0.5f * GRAVITY * j_dt * j_dt;

	if(m_y < PLAYER_Y_POS) {
		m_y = PLAYER_Y_POS;
		m_jumpState = NO_JUMP;
	}
}

void Player::jump() {
	m_jump = true;
}

void Player::endJump() {
	m_jump = false;
}
