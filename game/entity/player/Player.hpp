/*
 * Player.hpp
 *
 *  Created on: 2020. m�rc. 3.
 *      Author: korme
 */

#ifndef GAME_ENTITY_PLAYER_PLAYER_HPP_
#define GAME_ENTITY_PLAYER_PLAYER_HPP_

#include <game/entity/AnimatedEntity.hpp>


class Player : public AnimatedEntity {

public:

	Player();
	~Player();

	void update(float dt) override;

	void jump();
	void endJump();

	bool inAir() const;

protected:

	enum JumpState {
		NO_JUMP,
		JUMP,
		DOUBLE_JUMP
	};

	JumpState m_jumpState = NO_JUMP;

	float m_time = 0.0f;
	float m_jumpStartTime = 0.0f;

	bool m_jump = false;
	bool m_doubleJump = false;

	void updateAnimation(float dt) override;

};



#endif /* GAME_ENTITY_PLAYER_PLAYER_HPP_ */
