/*
 * IEntity.hpp
 *
 *  Created on: 2020. m�rc. 2.
 *      Author: korme
 */

#ifndef GAME_ENTITY_ENTITY_HPP_
#define GAME_ENTITY_ENTITY_HPP_

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <game/textures/Texture2D.hpp>

#include <game/shapes/Box.hpp>

class Entity {

public:

	Entity();
	virtual ~Entity();

	virtual void update(float dt) = 0;
	virtual void render() = 0;

	void setPosition(float x, float y);
	void setRotation(float angle);
	void setScale(float scale);
	void setScale(float scaleX, float scaleY);
	void setVelocity(float velocity);

	bool isLeftBehind() const;
	bool testCollision(const Entity& entity) const;
	int getNumHitboxes() const;
	int getHitboxData(Box::Vertices* d) const;

protected:

	struct Hitbox {
		float xOffset, yOffset;
		float width, height;

		Hitbox(float xOffset, float yOffset, float width, float height) :
			xOffset(xOffset), yOffset(yOffset), width(width), height(height) {

		}

		float getMinX(const Entity& e) const {
			return e.m_x + xOffset - width / 2.0f;
		}

		float getMaxX(const Entity& e) const {
			return e.m_x + xOffset + width / 2.0f;
		}

		float getMinY(const Entity& e) const {
			return e.m_y + yOffset + height / -2.0f;
		}

		float getMaxY(const Entity& e) const {
			return e.m_y + yOffset + height / 2.0f;
		}

	};

	float m_x = 0.0f, m_y = 0.0f;
	float m_angle = 0.0f;
	float m_scale_x = 1.0f;
	float m_scale_y = 1.0f;
	float m_velocity = 0.0f;

	std::vector<Hitbox> m_hitboxes;

	glm::mat4 getModelMatrix();

};



#endif /* GAME_ENTITY_ENTITY_HPP_ */
