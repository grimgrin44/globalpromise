/*
 * Entity.cpp
 *
 *  Created on: 2020. m�rc. 3.
 *      Author: korme
 */

#include "Entity.hpp"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <game/textures/Texture2D.hpp>
#include <game/shapes/Box.hpp>

Entity::Entity() {

}

Entity::~Entity() {

}

void Entity::setPosition(float x, float y) {
	m_x = x;
	m_y = y;
}

void Entity::setRotation(float angle) {
	m_angle = angle;
}

void Entity::setScale(float scale) {
	m_scale_x = m_scale_y = scale;
}

void Entity::setScale(float scaleX, float scaleY) {
	m_scale_x = scaleX;
	m_scale_y = scaleY;
}

void Entity::setVelocity(float velocity) {
	m_velocity = velocity;
}

bool Entity::isLeftBehind() const {
	return m_x < -1000.0f;
}

glm::mat4 Entity::getModelMatrix() {
	glm::mat4 m = glm::identity<glm::mat4>();
	m = glm::translate(m, glm::vec3(m_x, m_y, 0));
	m = glm::rotate(m, m_angle, glm::vec3(0, 0, 1));
	m = glm::scale(m, glm::vec3(m_scale_x, m_scale_y, 1));
	return m;
}

bool Entity::testCollision(const Entity& entity) const {
	for(const Hitbox& h1 : m_hitboxes) {
		for(const Hitbox& h2 : entity.m_hitboxes) {
			if( (h1.getMinX(*this) < h2.getMaxX(entity)) &&
				(h1.getMaxX(*this) > h2.getMinX(entity)) &&
				(h1.getMinY(*this) < h2.getMaxY(entity)) &&
				(h1.getMaxY(*this) > h2.getMinY(entity))) {
				return true;
			}
		}
	}
	return false;
}

int Entity::getNumHitboxes() const {
	return m_hitboxes.size();
}

int Entity::getHitboxData(Box::Vertices* d) const {
	for(const Hitbox& h : m_hitboxes) {
		glm::vec2 h_pos(m_x + h.xOffset, m_y + h.yOffset);
		Box b(h_pos, h.width, h.height, glm::vec3(1.0f, 0.0f, 0.0f));
		b.getVertices(d++);
	}
	return m_hitboxes.size();
}
