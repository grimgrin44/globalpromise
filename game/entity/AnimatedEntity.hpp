/*
 * AnimatedEntity.hpp
 *
 *  Created on: 2020. m�rc. 2.
 *      Author: korme
 */

#ifndef GAME_ENTITY_ANIMATEDENTITY_HPP_
#define GAME_ENTITY_ANIMATEDENTITY_HPP_

#include "Entity.hpp"

#include <vector>
#include <game/AssetManager.hpp>
#include <game/textures/Texture2D.hpp>
#include <game/shapes/Quad.hpp>

class AnimatedEntity : public Entity {

public:

	AnimatedEntity(unsigned int width, unsigned int height);
	AnimatedEntity(Shader* shader, unsigned int width, unsigned int height);
	virtual ~AnimatedEntity();

	void render() override;

protected:

	Shader* m_shader = nullptr;
	Quad m_quad;
	std::vector<Texture2D*> m_textures;

	float m_animationDuration = 0.1f;			//!< Animation duration
	float m_animationTime = 0.0f;
	unsigned int m_index = 0;

	void next();
	void reset();

	virtual void updateAnimation(float dt);

};



#endif /* GAME_ENTITY_ANIMATEDENTITY_HPP_ */
