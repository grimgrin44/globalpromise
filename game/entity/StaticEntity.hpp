/*
 * StaticEntity.hpp
 *
 *  Created on: 2020. m�rc. 2.
 *      Author: korme
 */

#ifndef GAME_ENTITY_STATICENTITY_HPP_
#define GAME_ENTITY_STATICENTITY_HPP_

#include "Entity.hpp"

#include <game/shader/Shader.hpp>
#include <game/textures/Texture2D.hpp>
#include <game/shapes/TexturedQuad.hpp>

class StaticEntity : public Entity {

public:

	StaticEntity();
	StaticEntity(Texture2D* texture, Shader* shader, unsigned int width, unsigned int height);
	virtual ~StaticEntity();

	void update(float dt);
	void render();

protected:

	Shader* m_shader = nullptr;
	TexturedQuad* m_object = nullptr;

};


#endif /* GAME_ENTITY_STATICENTITY_HPP_ */
