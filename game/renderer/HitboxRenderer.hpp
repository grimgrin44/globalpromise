/*
 * HitboxRenderer.hpp
 *
 *  Created on: 2020. m�rc. 8.
 *      Author: korme
 */

#ifndef GAME_RENDERER_HITBOXRENDERER_HPP_
#define GAME_RENDERER_HITBOXRENDERER_HPP_

#include <list>
#include <game/shapes/Box.hpp>
#include <game/entity/Entity.hpp>

class HitboxRenderer {

public:

	HitboxRenderer(int maxHitboxes = 100);
	~HitboxRenderer();

	void reset();
	void addHitboxes(const std::list<Entity*>& entities);
	void addHitbox(const Entity* entity);
	void render();

protected:

	int m_maxHitboxes;
	float* m_buffer;
	GLuint m_vao;
	GLuint m_vbos[2];
	int m_hitboxCount = 0;

};



#endif /* GAME_RENDERER_HITBOXRENDERER_HPP_ */
