/*
 * HitboxRenderer.cpp
 *
 *  Created on: 2020. m�rc. 8.
 *      Author: korme
 */

#include "HitboxRenderer.hpp"

HitboxRenderer::HitboxRenderer(int maxHitboxes) : m_maxHitboxes(maxHitboxes) {

	int bufferSize = maxHitboxes * sizeof(Box::Vertices);
	m_buffer = new float[bufferSize / sizeof(float)];
	m_hitboxCount = 0;

	glGenVertexArrays(1, &m_vao);
	glBindVertexArray(m_vao);
	glGenBuffers(2, m_vbos);
	glBindBuffer(GL_ARRAY_BUFFER, m_vbos[0]);
	glBufferData(GL_ARRAY_BUFFER, bufferSize, nullptr, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*) 0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*) (2 * sizeof(float)));
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	unsigned int* indices = new unsigned int[maxHitboxes * 8];
	unsigned int* ptr = indices;
	for(int i = 0; i < maxHitboxes; i++) {
		unsigned int idx = i * 4;
		*ptr++ = idx++;		// 0 + i * 4
		*ptr++ = idx;		// 1 + i * 4
		*ptr++ = idx++;		// 1 + i * 4
		*ptr++ = idx;		// 2 + i * 4
		*ptr++ = idx++;		// 2 + i * 4
		*ptr++ = idx;		// 3 + i * 4
		*ptr++ = idx;		// 3 + i * 4
		*ptr++ = i * 4;		// 0 + i * 4
	}

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_vbos[1]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, maxHitboxes * 8 * sizeof(unsigned int), indices, GL_STATIC_DRAW);
	glBindVertexArray(0);
}

HitboxRenderer::~HitboxRenderer() {
	glDeleteBuffers(2, m_vbos);
	glDeleteVertexArrays(1, &m_vao);
	delete[] m_buffer;
}

void HitboxRenderer::reset() {
	m_hitboxCount = 0;
}

void HitboxRenderer::addHitboxes(const std::list<Entity*> &entities) {
	Box::Vertices* v = (Box::Vertices*) m_buffer;
	for(const Entity* e : entities) {
		if(e->getNumHitboxes() == 0) {
			continue;
		}
		// Update data here
		if((m_hitboxCount + e->getNumHitboxes()) > m_maxHitboxes) {
			break;
		}
		int cnt = e->getHitboxData(v);
		m_hitboxCount += cnt;
		v += cnt;
		if(m_hitboxCount >= m_maxHitboxes) {
			break;
		}
	}
}

void HitboxRenderer::addHitbox(const Entity* entity) {
	if(entity->getNumHitboxes() > 0 && (m_hitboxCount + entity->getNumHitboxes()) <= m_maxHitboxes) {
		Box::Vertices* v = ((Box::Vertices*) m_buffer) + m_hitboxCount;
		m_hitboxCount += entity->getHitboxData(v);
	}
}

void HitboxRenderer::render() {
	if(m_hitboxCount > 0) {
		glBindVertexArray(m_vao);
		glBindBuffer(GL_ARRAY_BUFFER, m_vbos[0]);
		glBufferSubData(GL_ARRAY_BUFFER, 0, m_hitboxCount * sizeof(Box::Vertices), m_buffer);
		glDrawElements(GL_LINES, m_hitboxCount * 8, GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);
	}
}
