/*
 * MainMenu.hpp
 *
 *  Created on: 2020. m�rc. 1.
 *      Author: korme
 */

#ifndef GAME_STATES_MAINMENU_HPP_
#define GAME_STATES_MAINMENU_HPP_

#include <game/GameState.hpp>

class Game;

class MainMenu : public GameState {

public:

	MainMenu(Game& game);
	virtual ~MainMenu();

	void update(float delta);
	void render();
	GameState* nextState();

protected:

	Game& m_game;

};


#endif /* GAME_STATES_MAINMENU_HPP_ */
