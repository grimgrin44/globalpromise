/*
 * Configuration.hpp
 *
 *  Created on: 2020. m�rc. 4.
 *      Author: korme
 */

#ifndef GAME_CONFIGURATION_HPP_
#define GAME_CONFIGURATION_HPP_

#define GLOBAL_SPEED_FACTOR			1.0f

#define BACKGROUND_SPEED_FACTOR		0.5f

#define BACKGROUND_SCOLL_SPEED		(500.0f * BACKGROUND_SPEED_FACTOR * GLOBAL_SPEED_FACTOR)
#define RAT_SPEED					(700.0f * GLOBAL_SPEED_FACTOR)
#define PIG_SPEED					(600.0f * GLOBAL_SPEED_FACTOR)
#define DRAGON_SPEED				(900.0f * GLOBAL_SPEED_FACTOR)

#define MAX_NUM_ENEMIES				6

#define GRAVITY						-400.0f
#define JUMP_SPEED					140.0f
#define NO_GRAVITY_TIME				0.3f

#define PLAYER_Y_POS				50.0f
#define PLAYER_X_POS				80.0f



#endif /* GAME_CONFIGURATION_HPP_ */
