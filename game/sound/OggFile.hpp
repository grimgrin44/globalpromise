/*
 * OggFile.hpp
 *
 *  Created on: 2020. m�rc. 8.
 *      Author: korme
 */

#ifndef GAME_SOUND_OGGFILE_HPP_
#define GAME_SOUND_OGGFILE_HPP_

#include <string>
#include <vector>
#include <string.h>
#include <ogg/ogg.h>
#include <vorbis/codec.h>
#include <vorbis/vorbisfile.h>
#include <al.h>

class OggFile {

public:

	OggFile(const std::string& file);
	~OggFile();

	ALvoid* getAudioData() const;
	ALsizei getAudioDataLength() const;

protected:

	std::string m_file;
	std::vector<char> m_audioData;


	bool decode();


};



#endif /* GAME_SOUND_OGGFILE_HPP_ */
