/*
 * OggFile.cpp
 *
 *  Created on: 2020. m�rc. 8.
 *      Author: korme
 */

#include "OggFile.hpp"
#include <utility/FileUtils.hpp>
#include <iostream>

static size_t _read_vorbis_cb(void* ptr, size_t size, size_t memb, void* datasource) {
	Utils::FileStream* stream = static_cast<Utils::FileStream*>(datasource);
	return stream->read((char*)ptr, size * memb);
}

static int _seek_vorbis_cb(void* datasource, ogg_int64_t offset, int whence) {
	Utils::FileStream* stream = static_cast<Utils::FileStream*>(datasource);
	return stream->seek(offset, whence);
}

static long _tell_vorbis_cb(void* datasource) {
	Utils::FileStream* stream = static_cast<Utils::FileStream*>(datasource);
	return stream->tell();
}

static int _close_vorbis_cb(void* datasource) {
	Utils::FileStream* stream = static_cast<Utils::FileStream*>(datasource);
	delete stream;
	return 0;
}

OggFile::OggFile(const std::string& file) : m_file(file) {
	decode();
}

OggFile::~OggFile() {

}

ALvoid* OggFile::getAudioData() const {
	return (ALvoid*) m_audioData.data();
}

ALsizei OggFile::getAudioDataLength() const {
	return m_audioData.size();
}

bool OggFile::decode() {
	ogg_sync_state oy;
	ogg_page og;

	Utils::FileStream* stream = Utils::FileStream::create(m_file);
	OggVorbis_File vf;

	ov_callbacks cbs = {
		_read_vorbis_cb, 
		_seek_vorbis_cb, 
		_close_vorbis_cb, 
		_tell_vorbis_cb
	};

	if (ov_open_callbacks(stream, &vf, NULL, 0, cbs) < 0) {
		std::cerr << "Input doesn not appear to be an ogg bitstream." << std::endl;
		ov_clear(&vf);
		return false;
	}

	vorbis_info* vi = ov_info(&vf, -1);
	std::cout << m_file << " information:" << std::endl;
	std::cout << "Bitstream channel: " << vi->channels << " sample rate: " << vi->rate << "Hz" << std::endl;
	std::cout << "Decoded length: " << ov_pcm_total(&vf, -1) << " samples" << std::endl;
	std::cout << "Encoded by: " << ov_comment(&vf, -1)->vendor << std::endl;

	char* buffer = new char[65536];
	int currentSection = 0;
	bool eof = false;
	while (!eof) {
		long ret = ov_read(&vf, buffer, 65536, 0, 2, 1, &currentSection);
		if (ret == 0) {
			eof = true;
		} else if(ret < 0) {
			std::cerr << "Error occured during reading from stream. " << ret << std::endl;
			break;
		} else {
			m_audioData.insert(m_audioData.end(), buffer, buffer + ret);
		}
	}

	delete[] buffer;
	ov_clear(&vf);
	return false;
}

