#include "AudioSource.hpp"


AudioSource::AudioSource(OggFile* ogg, bool loop) : m_ogg(ogg) {
	alGenSources(1, &m_source);
	alGenBuffers(1, &m_sampleSet);
	alBufferData(m_sampleSet, AL_FORMAT_STEREO16, ogg->getAudioData(), ogg->getAudioDataLength(), 44100);
	alSourcei(m_source, AL_BUFFER, m_sampleSet);
	alSourcei(m_source, AL_LOOPING, ((loop) ? AL_TRUE : AL_FALSE));
}

AudioSource::~AudioSource() {
	alDeleteSources(1, &m_source);
	alDeleteBuffers(1, &m_sampleSet);
}

void AudioSource::setVolume(float vol) {
	alSourcef(m_source, AL_GAIN, vol);
}

void AudioSource::play() {
	alSourcePlay(m_source);
}

void AudioSource::stop() {
	alSourceStop(m_source);
}