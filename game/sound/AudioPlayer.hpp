/*
 * AudioPlayer.hpp
 *
 *  Created on: 2020. m�rc. 8.
 *      Author: korme
 */

#ifndef GAME_SOUND_AUDIOPLAYER_HPP_
#define GAME_SOUND_AUDIOPLAYER_HPP_

#include <al.h>
#include <alc.h>

class AudioPlayer {

public:

	AudioPlayer();
	~AudioPlayer();

protected:

	ALCdevice* m_device = nullptr;
	ALCcontext* m_context = nullptr;

};



#endif /* GAME_SOUND_AUDIOPLAYER_HPP_ */
