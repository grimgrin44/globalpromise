#pragma once

#include <al.h>
#include "OggFile.hpp"

class AudioSource {

public:

	AudioSource(OggFile* ogg, bool loop = false);
	~AudioSource();

	void play();
	void stop();

	void setVolume(float vol);

protected:

	OggFile* m_ogg;
	ALuint m_source;
	ALuint m_sampleSet;
};