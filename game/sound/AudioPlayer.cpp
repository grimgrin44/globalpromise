/*
 * AudioPlayer.cpp
 *
 *  Created on: 2020. m�rc. 8.
 *      Author: korme
 */

#include "AudioPlayer.hpp"

#include <iostream>

AudioPlayer::AudioPlayer() {
	m_device = alcOpenDevice(nullptr);
	if(!m_device) {
		std::cerr << "Failed to open sound device" << std::endl;
	} else {
		m_context = alcCreateContext(m_device, NULL);
		alcMakeContextCurrent(m_context);
		alGetError();
	}
}

AudioPlayer::~AudioPlayer() {
	alcMakeContextCurrent(NULL);
	alcDestroyContext(m_context);
	alcCloseDevice(m_device);
}


