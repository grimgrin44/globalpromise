/*
 * Game.hpp
 *
 *  Created on: 2020. m�rc. 1.
 *      Author: korme
 */

#ifndef GAME_GAME_HPP_
#define GAME_GAME_HPP_

#include <map>
#include <string>
#include <list>

#include "Display.hpp"

#include "shader/Shader.hpp"
#include "shader/SimpleTextureShader.hpp"
#include "textures/Image.hpp"
#include "textures/Texture2D.hpp"
#include "shapes/TexturedQuad.hpp"
#include "shapes/Triangle.hpp"
#include "entity/StaticEntity.hpp"

#include <game/text/TextRenderer.hpp>

#include <game/entity/enemies/Rat.hpp>
#include <game/entity/enemies/Dragon.hpp>
#include <game/entity/enemies/Predator.hpp>
#include <game/entity/enemies/Pig.hpp>
#include <game/entity/player/Player.hpp>

#include <game/entity/collectable/Euro.hpp>

#include <game/renderer/HitboxRenderer.hpp>

#include <game/Background.hpp>

#include <game/sound/AudioPlayer.hpp>
#include <game/sound/AudioSource.hpp>

class Game {

public:

	Game();
	virtual ~Game();

	void init();

	void update(float dt);
	void draw();

	static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);

protected:

	enum State {
		START,
		GAME,
		GAME_OVER
	};

	Display* m_display;

	State m_state = START;
	bool m_showHitboxes = false;
	bool m_doUpdate = true;

	float m_time = 0.0f;
	float m_worldSpeed = 200.0f;

	float m_lastGenerateTime = 0.0f;
	float m_generateDelay = 1.0f;

	HitboxRenderer m_hitboxRenderer;

	Background* m_background = nullptr;

	Player* m_player = nullptr;
	std::list<Entity*> m_entities;
	std::list<Euro*> m_euros;

	StaticEntity* m_startLabel;
	TextRenderer* m_textRenderer;
	Text* m_timeLabel;
	Text* m_collisionLabel;

	AudioPlayer* m_audioPlayer;
	AudioSource* m_themeSound;
	AudioSource* m_coinSound;

	static Game* instance;

	void generateRandomEnemy();

	void update_START(float dt);
	void update_GAME(float dt);
	void update_GAME_OVER(float dt);

	void render_START();
	void render_GAME();
	void render_GAME_OVER();

	void onKeyEvent(int key, int scancode, int action, int mods);
};


#endif /* GAME_GAME_HPP_ */
