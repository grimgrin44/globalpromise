/*
 * Game.cpp
 *
 *  Created on: 2020. m�rc. 1.
 *      Author: korme
 */

#include "Game.hpp"

#include <iterator>
#include <glm/gtc/matrix_transform.hpp>

#include "AssetManager.hpp"
#include "Configuration.hpp"

#include <game/entity/collectable/Euro.hpp>

#define ENEMY_START_POS_X		1500.0f

#define RAT_START_POS_Y			70.0f
#define DRAGON_START_POS_Y		700.0f
#define PIG_START_POS_Y			120.0f

Game* Game::instance = nullptr;

void Game::keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
	if(instance) {
		instance->onKeyEvent(key, scancode, action, mods);
	}
}

Game::Game() {
	instance = this;
	m_display = Display::getDisplay();
}

Game::~Game() {
	delete m_coinSound;
	delete m_themeSound;
	delete m_audioPlayer;
}

void Game::init() {
	AssetManager::init();
	AssetManager* am = AssetManager::getInstance();

	m_background = new Background();
	m_background->setScrollSpeed(BACKGROUND_SCOLL_SPEED);

	m_player = new Player();
	m_player->setPosition(PLAYER_X_POS, PLAYER_Y_POS);

	m_textRenderer = new TextRenderer();
	m_timeLabel = new Text("0", 10, 700, 14, "consolas");
	m_collisionLabel = new Text("", 10, 686, 14, "consolas");
	m_textRenderer->addText(m_timeLabel);
	m_textRenderer->addText(m_collisionLabel);

	m_startLabel = new StaticEntity(am->getTexture("menu_go_one"), am->getShader("simple_texture"), 400, 50);
	m_startLabel->setPosition(m_display->getWidth() / 2, m_display->getHeight() / 2);
	m_startLabel->setRotation(0.4f);

	m_audioPlayer = new AudioPlayer();
	m_themeSound = new AudioSource(am->getOgg("theme"), true);
	m_themeSound->play();

	m_coinSound = new AudioSource(am->getOgg("coin"));
	m_coinSound->setVolume(0.5f);

	m_time = (float) glfwGetTime();
}

void Game::update(float dt) {

	if(!m_doUpdate) {
		return;
	}

	m_time += dt;

	switch(m_state) {

	case START:
		update_START(dt);
		break;

	case GAME:
		update_GAME(dt);
		break;

	case GAME_OVER:
		update_GAME_OVER(dt);
		break;

	default:
		m_state = START;
		break;

	}

	m_timeLabel->setText("Time: " + std::to_string(m_time));

}

void Game::draw() {

	m_background->render();

	switch(m_state) {

	case START:
		render_START();
		break;

	case GAME:
		render_GAME();
		break;

	case GAME_OVER:
		render_GAME_OVER();
		break;

	default:
		m_state = START;
		break;

	}

	m_textRenderer->render();
}

void Game::generateRandomEnemy() {
	int v = rand() % 100;
	if(v < 50) {
		Rat* rat = new Rat();
		rat->setPosition(ENEMY_START_POS_X, RAT_START_POS_Y + (rand() % 30) - 15);
		m_entities.push_back(rat);
	} else if(v >= 50 && v <= 65) {
		Pig* pig = new Pig();
		pig->setPosition(ENEMY_START_POS_X, PIG_START_POS_Y);
		m_entities.push_back(pig);
	} else {
		Dragon* dragon = new Dragon();
		dragon->setPosition(ENEMY_START_POS_X, DRAGON_START_POS_Y + (rand() % 30) - 60);
		m_entities.push_back(dragon);
	}
}

void Game::update_START(float dt) {
	m_background->update(dt);
	m_startLabel->setScale(0.5f * sinf(3.0f * m_time) + 2.0f);
}

void Game::update_GAME(float dt) {

	for(Entity* e : m_entities) {
		e->update(dt);
	}

	for(Euro* e : m_euros) {
		e->update(dt);
	}

	m_player->update(dt);
	m_background->update(dt);

	if(m_showHitboxes) {
		m_hitboxRenderer.reset();
		m_hitboxRenderer.addHitboxes(m_entities);
		m_hitboxRenderer.addHitbox(m_player);
		for(Euro* e : m_euros) {
			if(!e->isCollected()) {
				m_hitboxRenderer.addHitbox(e);
			}
		}
	}

	m_entities.remove_if([](Entity* e) {
		if(e->isLeftBehind()) {
			delete e;
			return true;
		}
		return false;
	});

	m_euros.remove_if([](Euro* e) {
		if(e->isLeftBehind()) {
			delete e;
			return true;
		}
		return false;
	});

	if((m_time - m_lastGenerateTime) > m_generateDelay) {
		m_lastGenerateTime = m_time;
		if(m_entities.size() < MAX_NUM_ENEMIES) {
			generateRandomEnemy();
		}

		if(m_euros.size() < MAX_NUM_ENEMIES) {
			Euro* e = new Euro();
			e->setPosition(1500, 100);
			e->setVelocity(-BACKGROUND_SCOLL_SPEED);
			m_euros.push_back(e);
		}
	}

	bool collision = false;
	for(const Entity* e : m_entities) {
		if(m_player->testCollision(*e)) {
			collision = true;
			break;
		}
	}

	for(Euro* e : m_euros) {
		if(m_player->testCollision(*e) && !e->isCollected()) {
			e->collect();
			m_coinSound->play();
		}
	}

	m_collisionLabel->setText("Collision: " + std::to_string(collision));
}

void Game::update_GAME_OVER(float dt) {

}

void Game::render_START() {
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//m_testQuadShader->enable();
	Shader* s = AssetManager::getInstance()->getShader("simple_texture");
	s->enable();
	s->setUniform("projection", glm::ortho(0.0f, (float) m_display->getWidth(), 0.0f, (float) m_display->getHeight()));
	m_startLabel->render();
	glDisable(GL_BLEND);
}

void Game::render_GAME() {
	glm::mat4 projection = glm::ortho(0.0f, (float)m_display->getWidth(), 0.0f, (float)m_display->getHeight());
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//m_testQuadShader->enable();
	Shader* s = AssetManager::getInstance()->getShader("simple_texture");
	s->enable();
	s->setUniform("projection", projection);

	for(Entity* e : m_entities) {
		e->render();
	}

	for(Euro* e : m_euros) {
		if(!e->isCollected()) {
			e->render();
		}
	}

	m_player->render();
	glDisable(GL_BLEND);

	if(m_showHitboxes) {
		s = AssetManager::getInstance()->getShader("simple_color");
		s->enable();
		s->setUniform("projection", projection);
		m_hitboxRenderer.render();
	}
}

void Game::render_GAME_OVER() {

}

void Game::onKeyEvent(int key, int scancode, int action, int mods) {
	if(GLFW_KEY_SPACE == key) {
		if(action == GLFW_PRESS) {
			if(m_state == GAME) {
				m_player->jump();
			}
		} else if(action == GLFW_RELEASE) {
			if(m_state == GAME) {
				m_player->endJump();
			} else {
				m_state = GAME;
			}
		}
	} else if(GLFW_KEY_ESCAPE == key) {
		m_state = START;
	} else if(GLFW_KEY_F10 == key && action == GLFW_RELEASE) {
		m_showHitboxes = !m_showHitboxes;
	} else if(GLFW_KEY_U == key) {
		if(action == GLFW_PRESS) {
			m_doUpdate = true;
		} else if(action == GLFW_RELEASE) {
			m_doUpdate = false;
		}
	} else if(GLFW_KEY_R == key && action == GLFW_RELEASE) {
		m_doUpdate = true;
	} else if(GLFW_KEY_S == key && action == GLFW_RELEASE) {
		m_doUpdate = false;
	}
}
