/*
 * TextureShader.hpp
 *
 *  Created on: 2020. m�rc. 1.
 *      Author: korme
 */

#ifndef GAME_SHADER_SIMPLETEXTURESHADER_HPP_
#define GAME_SHADER_SIMPLETEXTURESHADER_HPP_

#include "Shader.hpp"
#include <glm/glm.hpp>

class SimpleTextureShader : public Shader {

public:

	SimpleTextureShader();
	virtual ~SimpleTextureShader();

	void setProjection(const glm::mat4& proj);

protected:

	GLint m_projection;

	void getAttributeLocations() override;

};



#endif /* GAME_SHADER_SIMPLETEXTURESHADER_HPP_ */
