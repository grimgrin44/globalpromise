/*
 * SimpleTextureShader.cpp
 *
 *  Created on: 2020. m�rc. 1.
 *      Author: korme
 */

#include "SimpleTextureShader.hpp"
#include <glm/gtc/type_ptr.hpp>

SimpleTextureShader::SimpleTextureShader() : Shader("assets/shaders/simple_texture.vert", "assets/shaders/simple_texture.frag") {
	getAttributeLocations();
}

SimpleTextureShader::~SimpleTextureShader() {

}

void SimpleTextureShader::getAttributeLocations() {
	m_projection = glGetUniformLocation(m_program, "projection");
}

void SimpleTextureShader::setProjection(const glm::mat4& proj) {
	glUniformMatrix4fv(m_projection, 1, GL_FALSE, glm::value_ptr(proj));
}
