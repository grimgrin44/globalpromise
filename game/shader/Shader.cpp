/*
 * Shader.cpp
 *
 *  Created on: 2020. m�rc. 1.
 *      Author: korme
 */

#include "Shader.hpp"
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include <iostream>

#include <utility/FileUtils.hpp>

Shader::Shader(const std::string& vertexShader, const std::string& fragmentShader) {
	char* vertSrc = Utils::loadFile(vertexShader);
	char* fragSrc = Utils::loadFile(fragmentShader);
	if(vertSrc && fragSrc) {
		GLuint vert = glCreateShader(GL_VERTEX_SHADER);
		GLuint frag = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(vert, 1, &vertSrc, nullptr);
		glShaderSource(frag, 1, &fragSrc, nullptr);

		if(!compileShader(vert)) {
			std::cerr << "Failed to compile vertex shader: " << vertexShader << std::endl;
			dumpShaderLog(vert);
		}

		if(!compileShader(frag)) {
			std::cerr << "Failed to compile fragment shader: " << fragmentShader << std::endl;
			dumpShaderLog(frag);
		}

		m_program = glCreateProgram();
		glAttachShader(m_program, vert);
		glAttachShader(m_program, frag);

		glLinkProgram(m_program);

		GLint isLinked = 0;
		glGetProgramiv(m_program, GL_LINK_STATUS, &isLinked);
		if(isLinked == GL_FALSE) {
			std::cerr << "Failed to link shader program" << std::endl;
			dumpProgramLog(m_program);
		}

		glDetachShader(m_program, vert);
		glDetachShader(m_program, frag);

		glDeleteShader(vert);
		glDeleteShader(frag);

		delete[] vertSrc;
		delete[] fragSrc;
	}
}

Shader::~Shader() {
	glDeleteProgram(m_program);
}

void Shader::enable() {
	glUseProgram(m_program);
}

void Shader::getAttributeLocations() {

}

void Shader::setUniform(const std::string& name, const glm::mat4& m) {
	if(m_locations.find(name) == m_locations.end()) {
		m_locations[name] = glGetUniformLocation(m_program, name.c_str());
	}
	glUniformMatrix4fv(m_locations[name], 1, GL_FALSE, glm::value_ptr(m));
}

void Shader::setUniform(const std::string& name, const glm::vec3& v) {
	if(m_locations.find(name) == m_locations.end()) {
		m_locations[name] = glGetUniformLocation(m_program, name.c_str());
	}
	glUniform3fv(m_locations[name], 1, glm::value_ptr(v));
}

bool Shader::compileShader(GLint shader) {
	GLint compiled;
	glCompileShader(shader);
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
	return (compiled != GL_FALSE);
}

void Shader::dumpShaderLog(GLint shader) {
	GLint logSize = 0;
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logSize);
	char* log = new char[logSize];
	glGetShaderInfoLog(shader, logSize, &logSize, log);
	std::cerr << log << std::endl;
	delete[] log;
}

void Shader::dumpProgramLog(GLint program) {
	GLint logSize = 0;
	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logSize);
	char* log = new char[logSize];
	glGetProgramInfoLog(program, logSize, &logSize, log);
	std::cerr << log << std::endl;
	delete[] log;
}
