/*
 * Shader.hpp
 *
 *  Created on: 2020. m�rc. 1.
 *      Author: korme
 */

#ifndef SHADER_HPP_
#define SHADER_HPP_

#include <string>
#include <map>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

class Shader {

public:

	Shader(const std::string& vertexShader, const std::string& fragmentShader);
	virtual ~Shader();

	void enable();

	void setUniform(const std::string& name, const glm::mat4& m);
	void setUniform(const std::string& name, const glm::vec3& v);

protected:

	GLuint m_program;
	std::map<std::string, GLint> m_locations;

	virtual void getAttributeLocations();

	static bool compileShader(GLint shader);
	static void dumpShaderLog(GLint shader);
	static void dumpProgramLog(GLint program);

};


#endif /* SHADER_HPP_ */
