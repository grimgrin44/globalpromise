/*
 * Background.hpp
 *
 *  Created on: 2020. m�rc. 3.
 *      Author: korme
 */

#ifndef GAME_BACKGROUND_HPP_
#define GAME_BACKGROUND_HPP_

#include <game/shapes/Quad.hpp>
#include <game/shader/Shader.hpp>
#include <game/textures/Texture2D.hpp>
#include <game/shapes/Quad.hpp>

class Background {

public:

	Background();
	~Background();

	void update(float dt);
	void render();

	void setScrollSpeed(float v);

protected:

	Texture2D* m_texture;
	Shader* m_shader;
	Quad* m_quads[2];

	float m_x[2];

	float m_scrollSpeed = 0.0f;

};



#endif /* GAME_BACKGROUND_HPP_ */
