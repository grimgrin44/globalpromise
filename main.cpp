/*
 * main.cpp
 *
 *  Created on: 2020. m�rc. 1.
 *      Author: korme
 */

#include <iostream>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <game/Display.hpp>
#include <game/Game.hpp>

void window_size_callback(GLFWwindow* window, int width, int height);

int main(int argc, char** argv) {

	if(!glfwInit()) {
		std::cerr << "Failed to initialize GLFW." << std::endl;
		return -1;
	}

	Display* display = Display::createDisplay("Global Promise: The game", 1280, 720);
	GLFWwindow* win = display->getWindow();
	if(win == NULL) {
		std::cerr << "Failed to create window. Exiting." << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(win);
	gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
	glViewport(0, 0, display->getWidth(), display->getHeight());
	glfwSetWindowSizeCallback(win, window_size_callback);

	glfwSwapInterval(1);

	Game* game = new Game();
	game->init();
	glfwSetKeyCallback(win, Game::keyCallback);
	float lastTime = glfwGetTime();
	while(!glfwWindowShouldClose(win)) {
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);
		game->draw();
		glfwSwapBuffers(win);

		float time = glfwGetTime();
		float dt = time - lastTime;
		//if(dt > 0.010f) {
			glfwPollEvents();
			lastTime = time;
			game->update(dt);
		//}
	}
	delete game;

	Display::destroy();
	glfwTerminate();

	return 0;
}

void window_size_callback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

