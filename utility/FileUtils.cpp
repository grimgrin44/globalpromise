/*
 * FileUtils.cpp
 *
 *  Created on: 2020. m�rc. 4.
 *      Author: korme
 */

#include "FileUtils.hpp"

#include <iostream>

#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <io.h>

namespace Utils {

	char* loadFile(const std::string& file, unsigned int* fileSize) {
		int fd = _open(file.c_str(), O_RDONLY | O_BINARY);
		if (fd < 0) {
			std::cerr << "Failed to open file: " << file << std::endl;
			return nullptr;
		}

		int size = _lseek(fd, 0, SEEK_END);
		_lseek(fd, 0, SEEK_SET);

		if (fileSize) {
			*fileSize = size;
		}

		char* buf = new char[size + 1];
		memset(buf, 0, size + 1);
		if (buf == nullptr) {
			_close(fd);
			return nullptr;
		}

		int readLength = 0;
		int ret = 0;

		do {
			int remaining = size - readLength;
			ret = _read(fd, buf + readLength, remaining);
			if (ret >= 0) {
				readLength += ret;
			}
		} while ((ret >= 0) && readLength < size);

		if (ret < 0) {
			std::cerr << "Failed to read from file: " << file << std::endl;
		}
		buf[size] = '\0';
		_close(fd);

		return buf;
	}

	FileStream* FileStream::create(const std::string& file) {
		unsigned int size = 0;
		char* data = loadFile(file, &size);
		return new FileStream(data, size, true);
	}

	FileStream::FileStream(char* data, unsigned int size, bool releaseOnDelete) : 
		m_data(data), m_size(size), m_releaseOnDelete(releaseOnDelete) {

	}

	FileStream::~FileStream() {
		delete[] m_data;
	}

	void FileStream::reset() const {
		m_currentIndex = 0;
	}

	unsigned int FileStream::seek(int64_t offset, int whence) const {
		if (whence == SEEK_SET) {
			m_currentIndex = offset;
		} else if(whence == SEEK_CUR) {
			m_currentIndex += offset;
		} else if(whence == SEEK_END) {
			m_currentIndex = m_size + offset;
		}
		return 0;
	}

	unsigned int FileStream::tell() const {
		return m_currentIndex;
	}

	unsigned int FileStream::read(char* buffer, unsigned int length) const {
		if (m_currentIndex >= m_size) {
			return 0;
		}

		if ((m_currentIndex + length) > m_size) {
			length = m_size - m_currentIndex;
		}
		memcpy(buffer, m_data + m_currentIndex, length);
		m_currentIndex += length;
		return length;
	}

}

