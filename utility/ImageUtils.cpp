/*
 * ImageUtils.cpp
 *
 *  Created on: 2020. m�rc. 1.
 *      Author: korme
 */

#include "../utility/ImageUtils.hpp"

#include <lodepng/lodepng.h>

namespace Utils {

std::vector<unsigned char> ImageUtils::loadPNG(const std::string& file, ImageSize& size) {
	std::vector<unsigned char> buffer;
	std::vector<unsigned char> image;

	if(!lodepng::load_file(buffer, file)) {
		lodepng::State state;
		lodepng::decode(image, size.width, size.height, state, buffer);
	}

	return image;
}


}
