/*
 * ImageUtils.hpp
 *
 *  Created on: 2020. m�rc. 1.
 *      Author: korme
 */

#ifndef UTILITY_IMAGEUTILS_HPP_
#define UTILITY_IMAGEUTILS_HPP_

#include <iostream>
#include <vector>

namespace Utils {

struct ImageSize {
	unsigned int width;
	unsigned int height;
};

class ImageUtils {

public:

	static std::vector<unsigned char> loadPNG(const std::string& file, ImageSize& size);

};

}



#endif /* UTILITY_IMAGEUTILS_HPP_ */
