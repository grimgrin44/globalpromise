/*
 * FileUtils.hpp
 *
 *  Created on: 2020. m�rc. 4.
 *      Author: korme
 */

#ifndef UTILITY_FILEUTILS_HPP_
#define UTILITY_FILEUTILS_HPP_

#include <string>

namespace Utils {

	char* loadFile(const std::string& file, unsigned int* fileSize = nullptr);


	class FileStream {

	public:

		FileStream(char* data, unsigned int size, bool releaseOnDelete = false);

		~FileStream();

		void reset() const;
		unsigned int seek(int64_t offset, int whence) const;
		unsigned int tell() const;

		unsigned int read(char* buffer, unsigned int length) const;


		static FileStream* create(const std::string& file);

	private:

		char* m_data;
		unsigned int m_size;
		bool m_releaseOnDelete;
		mutable unsigned int m_currentIndex = 0;

	};

}



#endif /* UTILITY_FILEUTILS_HPP_ */
